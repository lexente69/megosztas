import numpy as np
import itProcess
import itPrint

df = itProcess.itLoad()
itProcess.dropNull(df)
itProcess.itSegment(df)
x, y = itProcess.itBreak(df)
iNum = 20
xTrain, xTest, yTrain, yTest = itProcess.itTestSplit(x, y)
xTrain, xTest, yTrain, yTest = itProcess.itIndex(xTrain, xTest, yTrain, yTest)
xTrain, xTest = itProcess.itEncode(xTrain, xTest)
#itPrint.itCompareSelector(iNum, xTrain.columns.tolist(), itPrint.Pearson(xTrain, yTrain, iNum, xTrain.columns.tolist()), itPrint.Chi(xTrain, yTrain, iNum), itPrint.TreeBased(xTrain, yTrain, iNum), itPrint.Recursive(xTrain, yTrain, iNum), itPrint.itLasso(xTrain, yTrain, iNum))
print('\n')
#itPrint.itRoc(xTrain, xTest, yTrain, yTest)
xSelTrain = xTrain[['G2','G1','failures','Walc','Mjob_at_home']]
xSelTest = xTest[['G2','G1','failures','Walc','Mjob_at_home']]
#yTest = yTest.apply(lambda x: int(x/2))
#yTrain = yTrain.apply(lambda x: int(x/2))
print('Logistic:')
itProcess.itLogistic(xSelTrain, yTrain, xSelTest, yTest)
print('KNeighbors:')
itProcess.itKNeighbors(xSelTrain, yTrain, xSelTest, yTest)
print('Decision Tree:')
itProcess.itDecisionTree(xSelTrain, yTrain, xSelTest, yTest)

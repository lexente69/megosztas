import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import scipy.stats

from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.preprocessing import MinMaxScaler
from sklearn.feature_selection import SelectFromModel
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression

from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.datasets import make_classification

def spam(i):
    print('EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE ', i, ' EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE')

def rowCount(df):
    print('Sor szam: ' + str(len(df)))

def columnCount(df):
    print('Oszlop szam: ' + str(len(df.columns)))

def nanCount(df):
    print(df.isnull().sum().sum())

def dataTypes(df):
    print(df.dtypes)

def maxValue(df):
    print(df.max())

def minValue(df):
    print(df.min())

def itHistogram(df):
    _, bin, _ = plt.hist(df['G1'])
    plt.plot(bin, ' ', color='black')
    plt.show()

def itTestogram(df):
    _, bins3, _ = plt.hist(df['G2'], 38, density=1, alpha=0.6, color='orange')
    mu3, sigma3 = scipy.stats.norm.fit(df['G2'])
    y3 = ((1 / (np.sqrt(2 * np.pi) * sigma3)) *
         np.exp(-0.5 * (1 / sigma3 * (bins3 - mu3))**2))
    plt.plot(bins3, y3, '--', color='black')
    plt.title('G2 Histogram',color='white')
    plt.tick_params(axis='x', colors='white')
    plt.tick_params(axis='y', colors='white')
    plt.show()

def itScatter(df):
    plt.scatter(df['G1'], df['G3'], c = 'blue')
    plt.xlabel("G1")
    plt.ylabel("G3")
    plt.show()

def Pearson(x, y, iNum, feature_name):
    cor_list = []
    # calculate the correlation with y for each feature
    for i in x.columns.tolist():
        cor = np.corrcoef(x[i], y)[0, 1]
        cor_list.append(cor)
    cor_feature = x.iloc[:,np.argsort(np.abs(cor_list))[-iNum:]].columns.tolist()
    # feature selection? 0 for not select, 1 for select
    cor_support = [True if i in cor_feature else False for i in feature_name]
    #print(cor_feature, ' are the Pearson correlation')
    return cor_support

def Chi(x, y, iNum):
    X_norm = MinMaxScaler().fit_transform(x)
    chi_selector = SelectKBest(chi2, k=iNum)
    chi_selector.fit(X_norm, y)
    chi_support = chi_selector.get_support()
    #chi_feature = x.loc[:,chi_support].columns.tolist()
    #print(chi_feature, ' are the Chi-squared')
    return chi_support

def TreeBased(x, y, iNum):
    embeded_rf_selector = SelectFromModel(RandomForestClassifier(n_estimators=10000), max_features=iNum)
    embeded_rf_selector.fit(x, y)
    embeded_rf_support = embeded_rf_selector.get_support()
    #embeded_rf_feature = x.loc[:,embeded_rf_support].columns.tolist()
    #print(embeded_rf_feature, 'are the Tree-based')
    return embeded_rf_support
    

def Recursive(x, y, iNum):
    X_norm = MinMaxScaler().fit_transform(x)
    rfe_selector = RFE(estimator=LogisticRegression(max_iter=400), n_features_to_select=iNum, step=10, verbose=5)
    rfe_selector.fit(X_norm, y)
    rfe_support = rfe_selector.get_support()
    #rfe_feature = x.loc[:,rfe_support].columns.tolist()
    #print(rfe_feature, ' are the RFE selected features')
    return rfe_support

def itCompareSelector(iNum, feature_name, cor_support, chi_support, embeded_rf_support, rfe_support, embeded_lr_support):
    feature_selection_x = pd.DataFrame({'Feature':feature_name, 'Pears':cor_support, 'Chi-2':chi_support, 'RanForest':embeded_rf_support,'RFE':rfe_support, 'Lasso':embeded_lr_support})
    feature_selection_x['Total'] = np.sum(feature_selection_x, axis=1)
    feature_selection_x = feature_selection_x.sort_values(['Total','Feature'] , ascending=False)
    feature_selection_x.index = range(1, len(feature_selection_x)+1)
    print(feature_selection_x.head(iNum))

def itLasso(x, y, iNum):
    X_norm = MinMaxScaler().fit_transform(x)
    embeded_lr_selector = SelectFromModel(LogisticRegression(penalty="none", max_iter=400), max_features=iNum)
    embeded_lr_selector.fit(X_norm, y)
    embeded_lr_support = embeded_lr_selector.get_support()
    return embeded_lr_support

def itDontWork(xTrain, xTest, yTrain, yTest):
    ns_probs = [0 for _ in range(len(yTest))]
    model = LogisticRegression(solver='lbfgs')
    model.fit(xTrain, yTrain)
    lr_probs = model.predict_proba(xTest)
    lr_probs = lr_probs[:, 1]
    ns_auc = roc_auc_score(yTest, ns_probs, multi_class='ovr')
    lr_auc = roc_auc_score(yTest, lr_probs, multi_class='ovr')
    print('No Skill: ROC AUC=%.3f' % (ns_auc))
    print('Logistic: ROC AUC=%.3f' % (lr_auc))
    ns_fpr, ns_tpr, _ = roc_curve(yTest, ns_probs)
    lr_fpr, lr_tpr, _ = roc_curve(yTest, lr_probs)
    plt.plot(ns_fpr, ns_tpr, linestyle='--', label='No Skill')
    plt.plot(lr_fpr, lr_tpr, marker='.', label='Logistic')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend()
    plt.show()

def itRoc(xTrain, xTest, yTrain, yTest):
    yTest = yTest.apply(lambda x: int(x/2))
    yTrain = yTrain.apply(lambda x: int(x/2))
    model = RandomForestClassifier()
    model.fit(xTrain, yTrain)
    probs = model.predict_proba(xTest)
    probs = probs[:, 1]
    auc = roc_auc_score(yTest, probs, multi_class='ovo')
    print('AUC: %.2f' % auc)
    fpr, tpr, thresholds = roc_curve(yTest, probs)
    plt.plot(fpr, tpr, color='orange', label='ROC')
    plt.plot([0, 1], [0, 1], color='darkblue', linestyle='--')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic (ROC) Curve')
    plt.legend()
    plt.show()
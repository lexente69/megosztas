import pandas as pd
import numpy as np
from scipy import sparse
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import recall_score


def itLoad():
    df = pd.read_csv('C:/Users/belev/Desktop/Random/project/VS/Testing/student-mat.csv',delimiter=";")
    return df

def dropNull(df):
    df.dropna()

def itIndex(a, b, c, d):
    a = a.reset_index(drop=True)
    b = b.reset_index(drop=True)
    c = c.reset_index(drop=True)
    d = d.reset_index(drop=True)
    return a, b, c, d

def itEncode(daf, pf):
    ohe = OneHotEncoder(drop='if_binary', sparse=False)
    obj_daf = daf.select_dtypes(include=['object']).copy()
    obj_pf = pf.select_dtypes(include=['object']).copy()
    colNames = pd.DataFrame(obj_daf).columns
    ohe.fit(obj_daf)
    codedaf = pd.DataFrame(ohe.transform(obj_daf))
    codepf = pd.DataFrame(ohe.transform(obj_pf))
    codedaf.columns = ohe.get_feature_names(colNames)
    codepf.columns = ohe.get_feature_names(colNames)
    daf.drop(colNames, axis=1, inplace=True)
    pf.drop(colNames, axis=1, inplace=True)
    daf = pd.concat([daf, codedaf], axis=1)
    pf = pd.concat([pf, codepf], axis=1)
    return daf, pf

def itSegment(df):
    df['G3'] = df['G3'].apply(lambda x: int(x/5.1))
    #df['G3'] = df['G3'].apply(lambda x: int(x/10.1))

def itBreak(df):
    y = df['G3']
    x = df.drop(columns=['G3'])
    return x, y

def itTestSplit(x, y):
    return train_test_split(x, y, test_size=0.30, stratify=y, random_state=76)

def itLogistic(xTrain, yTrain, xTest, yTest):
    matSize = yTrain.nunique()
    model = LogisticRegression(random_state=76, max_iter=400)
    model.fit(xTrain, yTrain)
    yNew = model.predict(xTest)
    sumTp = 0
    sumFp = 0
    sumFn = 0
    for selCol in range(matSize):
        conMat = [[0]*matSize for i in range(matSize)]
        tp = 0
        fp = 0
        tn = 0
        fn = 0
        for i in range(len(xTest)):
            conMat[yTest[i]][yNew[i]] += 1
            if yTest[i]==selCol:
                if yNew[i]==selCol:
                    tp += 1
                else:
                    fp += 1
            else:
                if yNew[i]==selCol:
                    fn += 1
                else:
                    tn += 1
        if selCol==0:
            print(conMat)
        #print(tp, fp, fn, tn)
        sumTp += tp
        sumFp += fp
        sumFn += fn
        accuracy = (tp+tn)/(tp+fp+fn+tn)
        recall = tp/(tp+fn)
        precision = tp/(tp+fp)
        F = 2*precision*recall/(precision+recall)
        print("Accuracy=%.2f, Recall=%.2f, Precision=%.2f, F1 score=%.2f" % ((accuracy*100), recall*100, precision*100, F*100))
    recall = sumTp/(sumTp+sumFn)
    precision = sumTp/(sumTp+sumFp)
    F = 2*precision*recall/(precision+recall)
    print("SumRecall=%.2f, SumPrecision=%.2f, SumF1 score=%.2f" % (recall*100, precision*100, F*100))
    #print("Accuracy: ",model.score(xTest, yTest) * 100,'Recall: ',recall_score(yTest, yNew, average='weighted')*100)

def itKNeighbors(xTrain, yTrain, xTest, yTest):
    matSize = yTrain.nunique()
    model = KNeighborsClassifier()
    model.fit(xTrain, yTrain)
    yNew = model.predict(xTest)
    sumTp = 0
    sumFp = 0
    sumFn = 0
    for selCol in range(matSize):
        conMat = [[0]*matSize for i in range(matSize)]
        tp = 0
        fp = 0
        tn = 0
        fn = 0
        for i in range(len(xTest)):
            conMat[yTest[i]][yNew[i]] += 1
            if yTest[i]==selCol:
                if yNew[i]==selCol:
                    tp += 1
                else:
                    fp += 1
            else:
                if yNew[i]==selCol:
                    fn += 1
                else:
                    tn += 1
        if selCol==0:
            print(conMat)
        #print(tp, fp, fn, tn)
        sumTp += tp
        sumFp += fp
        sumFn += fn
        accuracy = (tp+tn)/(tp+fp+fn+tn)
        recall = tp/(tp+fn)
        precision = tp/(tp+fp)
        F = 2*precision*recall/(precision+recall)
        print("Accuracy=%.2f, Recall=%.2f, Precision=%.2f, F1 score=%.2f" % ((accuracy*100), recall*100, precision*100, F*100))
    recall = sumTp/(sumTp+sumFn)
    precision = sumTp/(sumTp+sumFp)
    F = 2*precision*recall/(precision+recall)
    print("SumRecall=%.2f, SumPrecision=%.2f, SumF1 score=%.2f" % (recall*100, precision*100, F*100))
    #print("Accuracy: ",model.score(xTest, yTest) * 100)

def itDecisionTree(xTrain, yTrain, xTest, yTest):
    matSize = yTrain.nunique()
    model = DecisionTreeClassifier(random_state=76)
    model.fit(xTrain, yTrain)
    yNew = model.predict(xTest)
    sumTp = 0
    sumFp = 0
    sumFn = 0
    for selCol in range(matSize):
        conMat = [[0]*matSize for i in range(matSize)]
        tp = 0
        fp = 0
        tn = 0
        fn = 0
        for i in range(len(xTest)):
            conMat[yTest[i]][yNew[i]] += 1
            if yTest[i]==selCol:
                if yNew[i]==selCol:
                    tp += 1
                else:
                    fp += 1
            else:
                if yNew[i]==selCol:
                    fn += 1
                else:
                    tn += 1
        if selCol==0:
            print(conMat)
        #print(tp, fp, fn, tn)
        sumTp += tp
        sumFp += fp
        sumFn += fn
        accuracy = (tp+tn)/(tp+fp+fn+tn)
        recall = tp/(tp+fn)
        precision = tp/(tp+fp)
        F = 2*precision*recall/(precision+recall)
        print("Accuracy=%.2f, Recall=%.2f, Precision=%.2f, F1 score=%.2f" % ((accuracy*100), recall*100, precision*100, F*100))
    recall = sumTp/(sumTp+sumFn)
    precision = sumTp/(sumTp+sumFp)
    F = 2*precision*recall/(precision+recall)
    print("SumRecall=%.2f, SumPrecision=%.2f, SumF1 score=%.2f" % (recall*100, precision*100, F*100))
    #print("Accuracy: ",model.score(xTest, yTest) * 100)

